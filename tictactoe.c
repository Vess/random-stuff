#include <stdio.h>

void drawBoard(char board[], int w, int h) {
	for (int i = 0; i < w*h; i++) {
		if (board[i] == 0) {
			printf("%d ", i+1);
		} else {
			printf("%c ", board[i]);
		}
		
		if ((i + 1) % w == 0) {
			printf("\n");
		}
	}
}

int winchecks(char board[], int w, int h) {
	// horizontal
	for (int i = 0; i < w*h; i = i+w) {
		if (board[i] == 0) {
			continue;
		}
		if (board[i] == board[i+1] && board[i] == board[i+2]) {
			printf("%c won!!\n", board[i]);
			return 1;
		}
	}

	// vertical
	for (int i = 0; i < w; i = i + 1) {
		if (board[i] == 0 ){
			continue;
		}
		if (board[i] == board[i+w] && board[i] == board[i+2*w]) {
			printf("%c won!!\n", board[i]);
			return 1;
		}
	}

	// diagonals
	// currently only works with 3x3, too lazy to loopty it
	if (board[0] == 0 || board[2] == 0) {
		return 0;
	}
	if (board[0] == board[4] && board[0] == board[8]) {
		printf("%c THAT won!!\n", board[0]);
		return 1;
	}
	if (board[2] == board[4] && board[2] == board[6]) {
		printf("%c won!!\n", board[2]);
		return 1;
	}

	return 0;
}

int main() {
	// width and height
	int w, h;
	w = 3;
	h = 3;

	char board[w*h];
	char input;
	char turn = 'x';

	while (1) {
		drawBoard(board, w, h);
		
		printf("%c> ", turn);
		input = getchar();
		printf("\n");

		// clear stdin
		char c;
		while (c = getchar() != '\n') {
		}

		if (input <= 48 || input >= 58
				|| board[input-49] == 'x'
				|| board[input-49] == 'o') {
			printf("Invalid, please pick something else.\n");
			continue;
		}

		board[input-49] = turn;

		if (winchecks(board, w, h)) {
			drawBoard(board, w, h);
			break;
		}
		
		if (turn == 'x') {
			turn = 'o';
		} else {
			turn = 'x';
		}
	}
}
