#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "raylib.h"

#define WINDOW_X_Y 1000
#define CELL_SIZE 50
#define ARENA_X_Y WINDOW_X_Y/CELL_SIZE
#define SNAKE_ARRAY_SIZE ARENA_X_Y*ARENA_X_Y

#define draw_point(x, y, color) DrawRectangle(CELL_SIZE * (x - 1), CELL_SIZE * (y - 1), CELL_SIZE - 1, CELL_SIZE - 1, color)
#define snake_head snake[0]

typedef struct {
    int x;
    int y;
} point;

typedef enum {
	UP,
	DOWN,
	LEFT,
	RIGHT
} dir;

int snake_length = 0; // NOTE: LARGER BY ONE THAN THE SNAKE ARRAY FUCK
dir snake_direction = UP;
point snake[SNAKE_ARRAY_SIZE];
point fruit;

void init_snake(void) {
	for(; snake_length < 3; snake_length++) {
		snake[snake_length].x = ARENA_X_Y/2;
		snake[snake_length].y = snake_length + ARENA_X_Y/2;
	}
}

void draw_snake(void) { // without the head, the head will be draw later cuz it's colored :D
	for(int i = 1; i <= snake_length; i++) {
		draw_point(snake[i].x, snake[i].y, WHITE);

	}
}

void set_fruit_location(void) {
	fruit.x = (rand()%(ARENA_X_Y-1))+1; // what i have written here might seem
	fruit.y = (rand()%(ARENA_X_Y-1))+1; // very very dumb, but it prevents fruit to spawn on x or y=0
}

void update(void) {
	// Lose condition
	for (int i = 1; i <= snake_length-1; i++) {
		if (snake[i].x == snake_head.x && snake[i].y == snake_head.y) {
			exit(0);
		}
	}
	
	// fruit nom nom
	if (snake_head.x == fruit.x && snake_head.y == fruit.y) {
		snake[snake_length].x = snake[snake_length-2].x;
		snake[snake_length].y = snake[snake_length-2].y;
		snake_length++;
		set_fruit_location();
	}

	// move body
	for (int i = snake_length-1; i > 0; i--) {
		snake[i].x = snake[i-1].x;
		snake[i].y = snake[i-1].y;
	}

	int input;
	int key;
	while((input = GetKeyPressed()) != 0) {
		key = input;
	}
	switch(key) {
		case KEY_DOWN: if(snake_direction != UP) {
			snake_direction = DOWN;
		} break;
		case KEY_UP: if(snake_direction != DOWN) {
			snake_direction = UP;

		} break;
		case KEY_RIGHT: if(snake_direction != LEFT) {
			snake_direction = RIGHT;
		} break;
		case KEY_LEFT: if(snake_direction != RIGHT) {
			snake_direction = LEFT;
		} break;
	}

	switch(snake_direction) {
		case DOWN:	snake_head.y++; break;
		case UP:	snake_head.y--; break;
		case RIGHT:	snake_head.x++; break;
		case LEFT:	snake_head.x--; break;
	}

	// wrap around borders
	if (snake_head.x <= 0) {
		snake_head.x = ARENA_X_Y;
	} else if (snake_head.x > ARENA_X_Y) {
		snake_head.x = 1;
	}
	if (snake_head.y <= 0) {
		snake_head.y = ARENA_X_Y;
	} else if (snake_head.y > ARENA_X_Y) {
		snake_head.y = 1;
	}
}

int main(void) {
	SetTargetFPS(25);
	InitWindow(WINDOW_X_Y, WINDOW_X_Y, "snek");
	init_snake();
	srand(time(0));
	set_fruit_location();

	while (!WindowShouldClose()) {
		BeginDrawing();
		ClearBackground(BLACK);
		update();

		draw_point(fruit.x, fruit.y, RED);
		draw_snake();
		draw_point(snake_head.x, snake_head.y, GREEN); // yes the snake head is drawn twice, shut up
		printf("fruit: %d %d\n", fruit.x, fruit.y);
		printf("snake_head: %d %d\n", snake_head.x, snake_head.y);

		EndDrawing();
	}
	CloseWindow();
}
